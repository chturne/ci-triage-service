from dataclasses import dataclass, field
from enum import IntFlag, auto
import io
import re
from typing import Optional


class InfraFailureType(IntFlag):
    TIMEOUT = auto()
    CONTAINER_ERROR = auto()  # Anything relating to container issues...
    BOOT_FAILURE = auto()
    REGISTRATION_FAILURE = auto()
    TARFILE_BUG_FAILURE = auto()
    PDU_BUG = auto()
    STALE_CLIENT_BUG = auto()
    GITLAB_GENERIC_JOB_FAILURE = auto()
    WGET_404_ERROR = auto()
    MINIO_ERROR = auto()
    DEQP_PARSE_ERROR = auto()
    TIMEOUT_NONE_BUG = auto()
    JOB_TIMEOUT = auto()
    NO_SPACE_LEFT = auto()
    EXECUTOR_ISSUE = auto()
    UNAUTHORIZED_USE_ERROR = auto()
    DRM_TIMEDOUT_FAILURE = auto()
    UNKNOWN = auto()
    IGNORED = auto()


class MatcherTrait(IntFlag):
    # Instead of continuing to find other failure types in the job trace, stop proecessing as soon as this matcher matches.
    STOP_AFTER_FIRST_MATCH = auto()


@dataclass
class MatcherAction:
    regex: bytes
    classification_results: list[InfraFailureType]
    diagnostics: list[str]
    meta: Optional[MatcherTrait] = None


MATCHER_TABLE = [
    MatcherAction(
        regex=re.compile(b"Mismatch for 'tags'"),
        classification_results=[InfraFailureType.REGISTRATION_FAILURE],
        diagnostics=["Machine tags mismatch"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(b"Hit the timeout <Timeout first_console_activity.*--> Abort!"),
        classification_results=[InfraFailureType.BOOT_FAILURE],
        diagnostics=["First console activity timeout"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(b"Hit the timeout <Timeout first_console_activity.*--> Abort!"),
        classification_results=[InfraFailureType.BOOT_FAILURE],
        diagnostics=["Machine failed to boot"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(b"Hit the timeout <Timeout console_activity:.*--> Abort!"),
        classification_results=[InfraFailureType.BOOT_FAILURE],
        diagnostics=["Console activity timer failed"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(b"\[drm:amdgpu_job_timedout\]|timed out waiting for fd to be ready|\\*ERROR\\* ring.*timeout"),
        classification_results=[InfraFailureType.DRM_TIMEDOUT_FAILURE],
        diagnostics=["Test suite timed due to device or kernel failure"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(b"ERROR: Job failed: execution took longer than \\d+h\\d+m\\d+s seconds|ERROR: Job failed: execution took longer than"),
        classification_results=[InfraFailureType.TIMEOUT],
        diagnostics=["Test timed out"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(
            b"error committing container|error pushing image|Error processing tar file|http: server gave HTTP response to HTTPS client|writing blob: adding layer with blob .*: layer not known|Cannot connect to the Docker daemon at unix:///var/run/docker\.sock\. Is the docker daemon running\?"
        ),
        classification_results=[InfraFailureType.CONTAINER_ERROR],
        diagnostics=["Container build / runtime issue"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(b"tarfile.ReadError: file could not be opened successfully"),
        classification_results=[InfraFailureType.TARFILE_BUG_FAILURE],
        diagnostics=["Job folder sync bug detected"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(
            b'requests.exceptions.HTTPError: 404 Client Error: Not Found for url: https://gitlab.freedesktop.org|Job failed \(system failure\)|Uploading artifacts.*Bad Gateway|ERROR: Uploading artifacts as "archive" to coordinator.*413 Request Entity Too Large|ERROR: Uploading artifacts as "archive" to coordinator... error.*couldn\'t execute POST against|HTTP request sent, awaiting response... 500 Internal Server Error|error: RPC failed;|section_end:1631042648:upload_artifacts_on_failure'
        ),
        classification_results=[InfraFailureType.GITLAB_GENERIC_JOB_FAILURE],
        diagnostics=["Gitlab related infra failure"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(b"ERROR 404: Not Found"),
        classification_results=[InfraFailureType.WGET_404_ERROR],
        diagnostics=["wget fetch failure"],
    ),
    MatcherAction(
        regex=re.compile(b"mcli: <ERROR>"),
        classification_results=[InfraFailureType.MINIO_ERROR],
        diagnostics=["minio generic error"],
    ),
    MatcherAction(
        regex=re.compile(b"AttributeError: 'NoneType' object has no attribute 'set'|The function <function SnmpPDU.get_port_state .*> failed 3 times in a row"),
        classification_results=[InfraFailureType.PDU_BUG],
        diagnostics=["Failure interacting with the PDU"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(b"A client is already connected, re-try later!"),
        classification_results=[InfraFailureType.STALE_CLIENT_BUG],
        diagnostics=["A client is already connected, re-try later!"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(b"Error: Failed to parse dEQP"),
        classification_results=[InfraFailureType.DEQP_PARSE_ERROR],
        diagnostics=["dEQP caselists parse error"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(b"AttributeError: 'NoneType' object has no attribute 'timeouts'"),
        classification_results=[InfraFailureType.TIMEOUT_NONE_BUG],
        diagnostics=["Timeout none bug detected"],
        meta=MatcherTrait.STOP_AFTER_FIRST_MATCH,
    ),
    MatcherAction(
        regex=re.compile(b"No space left on device"),
        classification_results=[InfraFailureType.NO_SPACE_LEFT],
        diagnostics=["No space left"],
    ),
    MatcherAction(
        regex=re.compile(b"ValueError: The server failed to initiate a connection|Internal Server Error"),
        classification_results=[InfraFailureType.EXECUTOR_ISSUE],
        diagnostics=["Executor bug / issue"],
    ),
    MatcherAction(
        regex=re.compile(b"The machines exposed by this runner are only allowed to be used by Valve-authorized projects"),
        classification_results=[InfraFailureType.UNAUTHORIZED_USE_ERROR],
        diagnostics=["Unauthorized user error"],
    ),
]


@dataclass
class InfraFailureResults:
    types: list[InfraFailureType] = field(default_factory=list)
    diagnostics: list[str] = field(default_factory=list)

    def empty(self):
        return len(self.types) == 0


def check_for_known_failure(job_log: str):
    infra_failure = InfraFailureResults()
    for line in job_log.splitlines():
        for ma in MATCHER_TABLE:
            if _ := re.search(ma.regex, line):
                infra_failure.types.extend(ma.classification_results)
                infra_failure.diagnostics.extend(ma.diagnostics)
                if ma.meta is not None:
                    if ma.meta & MatcherTrait.STOP_AFTER_FIRST_MATCH:
                        return infra_failure
    return infra_failure
