#!/usr/bin/env python
# -*- mode: python -*-
# Copyright (C) 2022 Igalia, S.L.
"""CI triaging tool."""

import argparse
from collections import defaultdict
from collections.abc import Iterable
from datetime import datetime, timedelta
from enum import IntFlag, auto
import functools
from functools import partial
import glob
import io
import os
import pickle
from pprint import pformat  # noqa
from pprint import pprint as pp  # noqa
import re
import shutil
import subprocess
import sys
import timeit
from typing import Any, Dict
import unicodedata
import xml  # noqa
import xml.etree.ElementTree as ET  # noqa

from colorama import Fore, Style  # type: ignore
import gitlab  # type: ignore
import humanize  # type: ignore
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
import yaml

from triage_patterns import check_for_known_failure
from util import PathType

D = os.path.dirname(os.path.realpath(__file__))


class LogLevel(IntFlag):
    INFO = auto()
    DEBUG = auto()


LOG_LEVEL = LogLevel.DEBUG

naturalsize = partial(humanize.naturalsize, binary=True)


def _log(level, msg, meta=True, **kwargs):
    global LOG_LEVEL
    if level <= LOG_LEVEL:
        if meta:
            ts = datetime.utcnow().strftime("%c|")
            print(ts, msg, **kwargs)
        else:
            print(msg, **kwargs)


def ilog(msg, meta=True, **kwargs):
    _log(LogLevel.INFO, msg, meta, **kwargs)


def dlog(msg, meta=True, *args, **kwargs):
    _log(LogLevel.DEBUG, msg, meta, **kwargs)


class JobFormatter:
    def __init__(self, indent: int = 0, colors=False):
        self.indent = indent
        self.colors = colors

    def fmt_header(self):
        return "%s%-30s%-50s %-10s %-40s %-10s" % ("\t" * self.indent, "Created At", "Name", "Status", "Tags", "Queued/Runtime")

    def fmt(self, job):
        if self.colors:
            return self._fmt_job_color(job)
        else:
            return self._fmt_job_plain(job)

    def _job_name(self, job):
        job_name_parts = job.name.split("-")
        if job.runner is not None:
            runner = job.runner["description"]
        else:
            runner = ""
        if len(job_name_parts) >= 2:
            return "%s/%s-%s" % (runner, job_name_parts[0], job_name_parts[1])
        return "%s/%ss" % (runner, job.name)

    def _job_status(self, job):
        if hasattr(job, "infra_failure"):
            return "\t%s%s" % ("\t" * self.indent, job.infra_failure)
        if hasattr(job, "deqp_results"):
            results = deqp_fmt_results(job.deqp_results, indent=self.indent)
            if len(results) == 0:
                return "\t%s%s" % ("\t" * self.indent, "No failures")
            else:
                return results
        return "%s%s" % ("\t" * self.indent, "Completed normally")

    def _fmt_job_color(self, job):
        if job.status == "success":
            fore_color = Fore.GREEN
        elif job.status == "failed":
            fore_color = Fore.RED
        elif job.status == "running":
            fore_color = Fore.YELLOW
        else:
            fore_color = Fore.WHITE

        return f"%s%-30s%-50s {fore_color}%-10s{Style.RESET_ALL} %-40s %-10s\n%s\n%s\t%s" % (
            "\t" * self.indent,
            job_created_at_pretty(job),
            self._job_name(job),
            job.status,
            ",".join(job.tag_list),
            job_runtime(job),
            self._job_status(job),
            "\t" * self.indent,
            job.web_url,
        )

    def _fmt_job_plain(self, job):
        return "%s%-30s%-50s %-10s %-40s %-10s\n%s\n%s\t%s" % (
            "\t" * self.indent,
            job_created_at_pretty(job),
            self._job_name(job),
            job.status,
            ",".join(job.tag_list),
            job_runtime(job),
            self._job_status(job),
            "\t" * self.indent,
            job.web_url,
        )


def generate_job_key(job):
    runner_name = job.runner["description"]
    runner_parts = runner_name.split("-")
    if runner_parts[0] == "igalia":
        # igalia-b2c -> igalia normalization
        runner_parts = [runner_parts[0], *runner_parts[2:]]
    assert len(runner_parts) >= 2
    farm_name, runner_desc = runner_parts[:2]
    job_parts = job.name.split("-")
    # TODO: We need to enforce this naming style at higher level, too
    # easy for mistakes to creep in!
    assert len(job_parts) == 3
    (
        job_type,
        gpu_type,
        company,
    ) = job_parts
    # Strip off the concurrency parameters, since that makes the statistics messy
    # valve n/m -> valve
    company = company.strip().split(" ")[0]
    return slugify("%s-%s/%s-%s" % (farm_name, gpu_type, job_type, company))


def config_from_args(args: argparse.Namespace):
    with open(args.config_file, "r") as f:
        return yaml.safe_load(f)


def slugify(value):
    value = unicodedata.normalize("NFKD", value).encode("ascii", "ignore").decode("ascii")
    value = re.sub(r"[^\w\s/-]", "", value).strip().lower()
    return re.sub(r"[-\s]+", "-", value)


def rfc3339_format(dt: datetime) -> str:
    return dt.isoformat("T") + "Z"


def timedelta_from_args_delta(delta_str: str) -> timedelta:
    n, units = delta_str.split(" ")
    unit_n = int(n.strip())
    if units.strip().lower().startswith("minute"):
        return timedelta(minutes=unit_n)
    elif units.strip().lower().startswith("hour"):
        return timedelta(hours=unit_n)
    elif units.strip().lower().startswith("day"):
        return timedelta(days=unit_n)
    elif units.strip().lower().startswith("week"):
        return timedelta(weeks=unit_n)
    else:
        raise ValueError(delta_str)


def pipeline_created_at_iso(pipe) -> datetime:
    if pipe.created_at is not None:
        return datetime.fromisoformat(pipe.created_at[:-1])


def job_created_at_iso(job) -> datetime:
    if job.created_at is not None:
        return datetime.fromisoformat(job.created_at[:-1])


def job_finished_at_iso(job) -> datetime:
    if job.finished_at is not None:
        return datetime.fromisoformat(job.finished_at[:-1])


def job_runtime(job) -> str:
    if job.queued_duration is None or job.duration is None:
        return "N/A"
    return "%s/%s" % (humanize.naturaldelta(timedelta(seconds=job.queued_duration)), humanize.naturaldelta(timedelta(seconds=job.duration)))


def job_created_at_pretty(job) -> str:
    dt = job_created_at_iso(job)
    return dt.strftime("%Y-%m-%d %H:%M")


def delta_since_from_args(args: argparse.Namespace) -> datetime:
    td = timedelta_from_args_delta(args.since)
    return datetime.now() - td


def since_from_args(args: argparse.Namespace) -> datetime:
    return datetime.fromisoformat(args.since)


def before_from_args(args: argparse.Namespace) -> datetime:
    return datetime.fromisoformat(args.before)


def completed_pipelines(pipelines: Iterable[Any]):
    for pipe in pipelines:
        if pipe.status in ["manual", "success", "failed"]:
            yield pipe


def matching_jobs(jobs, project_config):
    result_jobs = []
    job_name = re.compile(project_config["job_name"])
    job_stage = re.compile(project_config["job_stage"])
    for job in jobs:
        if job_runtime(job) == "N/A":
            continue
        if not job_stage.match(job.stage):
            continue
        if not job_name.match(job.name):
            continue
        result_jobs.append(job)
    return result_jobs


def job_try_downloading_artifact(job, aname, save_filename):
    ilog("%s %s | retrieving %s..." % (job.created_at, job.web_url, aname), end="", flush=True)
    if os.path.exists(save_filename):
        ilog("cache hit", meta=False)
        return True
    # The Gitlab API is ridiculous in many ways, the one relevant here is that you have no way to ask if an artifact exists without just trying to grab it.
    try:
        os.makedirs(os.path.dirname(save_filename))
        with open(save_filename, "wb") as f:
            job.artifact(aname, streamed=True, action=f.write)
        ilog("fetched  %s" % naturalsize(os.path.getsize(save_filename)), meta=False)
        return True
    except KeyboardInterrupt:
        shutil.rmtree(os.path.dirname(save_filename), ignore_errors=False, onerror=None)
        ilog("\nExiting in response to keyboard interrupt")
        sys.exit(1)
    except gitlab.GitlabError as err:
        if err.response_code and err.response_code == 404:
            ilog("404", meta=False)
        else:
            ilog("unknown gitlab error", meta=False)
        shutil.rmtree(os.path.dirname(save_filename), ignore_errors=False, onerror=None)
        return False


def scatter_utf8_writer(bytes, scatter_list=[]):
    for sl in scatter_list:
        sl.write(bytes.decode("utf-8"))


def get_first_found_artifact_bytes(filenames, pjob, output):
    if not isinstance(output, list):
        output = list(output)
    writer = functools.partial(scatter_utf8_writer, scatter_list=output)
    for filename in filenames:
        try:
            pjob.artifact(filename, streamed=True, action=writer)
            return True
        except KeyboardInterrupt:
            ilog("\nExiting in response to keyboard interrupt")
            sys.exit(1)
        except gitlab.GitlabError as err:
            if not (err.response_code and err.response_code == 404):
                ilog("unknown gitlab error: %s" % (err))
            continue
    return False


DEQP_JOB_REGEXP = re.compile(r"^deqp-.*")


def is_deqp_job(job):
    return re.match(DEQP_JOB_REGEXP, job.name) is not None


def job_assets_dir(db, job):
    return os.path.join(os.path.dirname(db.path), str(job.id))


class DeqpResultType(IntFlag):
    # See deqp-runner/src/runner_results.rs
    Pass = auto()
    Fail = auto()
    Skip = auto()
    Crash = auto()
    Flake = auto()
    Warn = auto()
    Missing = auto()
    ExpectedFail = auto()
    UnexpectedPass = auto()
    Timeout = auto()


TestName = str
DeqpResults = Dict[DeqpResultType, list[TestName]]


def deqp_results_from_csv(reader: io.TextIOBase) -> DeqpResults:
    results: DeqpResults = defaultdict(list)
    for line in reader.read().splitlines():
        test_name, result = line.split(",")
        results[DeqpResultType[result]].append(test_name)
    return results


def deqp_fmt_results(results: DeqpResults, indent: int = 0) -> str:
    display = io.StringIO()
    for rt, tests in results.items():
        display.write("%s%-10s %-5d\n" % (indent * "\t", rt.name, len(tests)))
        if len(tests) <= 50:  # Don't be overly spammy
            for t in sorted(tests):
                display.write("%s%s\n" % ("\t" * (indent + 1), t))
    return display.getvalue()


def mark_pipeline_cache_complete(pipeline_dir: str):
    """Let future runs know we processed this entire pipeline, so we shouldn't need to start again."""
    if pipeline_dir is not None:
        with open(os.path.join(pipeline_dir, ".complete"), "w") as f:
            f.write("Completed on %s" % datetime.now())


def push_results_to_influx(results: Dict[str, Iterable], project_config) -> None:
    assert "influx" in project_config
    influx_config = project_config["influx"]
    client = InfluxDBClient(url=influx_config["url"], token=influx_config["access_token"])
    write_api = client.write_api(write_options=SYNCHRONOUS)
    sequence = []
    for key, jobs in results.items():
        runner, test = key.split("/")
        for job in jobs:
            if hasattr(job, "infra_failure"):
                sequence.append(
                    Point(f"infra-failure-{runner}")
                    .tag("test", test)
                    .field("failure_type", '|'.join([t.name for t in job.infra_failure.types]))
                    .time(job.created_at, WritePrecision.NS)
                )
            else:
                sequence.append(
                    Point(runner)
                    .tag("test", test)
                    .field("queue_duration", job.queued_duration / 60)
                    .field("duration", job.duration / 60)
                    .time(job.created_at, WritePrecision.NS)
                )
    ilog("Batch writing %d measurements to Influx..." % len(sequence))
    write_api.write(influx_config["bucket"], influx_config["org"], sequence)


def dir_size_in_bytes(dir):
    return int(subprocess.check_output(["du", "-s", dir]).split()[0].decode("utf-8"))


def evict_old_cache_directories(pipeline_dir: str, starting_cache_size_in_bytes: int, max_cache_size_in_bytes: int):
    assert starting_cache_size_in_bytes > max_cache_size_in_bytes

    du_output = subprocess.check_output(["du", "-d", "1", f"{pipeline_dir}"]).decode("utf-8")
    cache_dirs_with_size_in_bytes: list[tuple] = []
    for dir_report in du_output.splitlines():
        dir_report = re.sub(r"\s+", " ", dir_report)
        size_in_bytes, dirname = dir_report.split(" ")
        size_in_bytes = int(size_in_bytes)
        if not re.match(r"%s/pipeline-\d+" % pipeline_dir, dirname):
            continue
        cache_dirs_with_size_in_bytes.append((os.stat(dirname).st_mtime, size_in_bytes, dirname))

    running_cache_size_in_bytes = starting_cache_size_in_bytes
    victims = []
    for candidate in sorted(cache_dirs_with_size_in_bytes, key=lambda e: e[0]):
        victims.append(candidate)
        running_cache_size_in_bytes -= candidate[1]
        if running_cache_size_in_bytes < max_cache_size_in_bytes:
            break

    for victim in victims:
        print("Evicting", victim[2], victim[1], victim[0])
        shutil.rmtree(victim[2], ignore_errors=True)


def check_pipeline_cache_disk_usage(pipeline_dir: str, max_cache_size_in_bytes: int):
    usage_in_bytes = dir_size_in_bytes(pipeline_dir)
    ilog("Currently using %s/%s of the cache storage" % (naturalsize(usage_in_bytes), naturalsize(max_cache_size_in_bytes)))
    if usage_in_bytes >= max_cache_size_in_bytes:
        ilog("Using too much disk space (%s/%s), start the eviction" % (naturalsize(usage_in_bytes), naturalsize(max_cache_size_in_bytes)))
        evict_old_cache_directories(pipeline_dir, usage_in_bytes, max_cache_size_in_bytes)


def cache_pipeline(proj, pipe, pipeline_dir, project_config):
    ilog(f"Reporting on pipline #{pipe.id} for commit {pipe.sha[:8]} on branch {pipe.ref} from {pipe.created_at}", flush=True)
    jobs = list(matching_jobs(pipe.jobs.list(all=True), project_config))
    if len(jobs) == 0:
        print("\tno AMD deqp jobs")
        mark_pipeline_cache_complete(pipeline_dir)
        return
    for job in jobs:
        print("Caching job: ", job.name, job.web_url)
        # Retrieve the "project job" from the job, a little quirk of the Gitlab API
        pjob = proj.jobs.get(job.id, lazy=False)
        job_trace = pjob.trace()
        setattr(job, "trace", job_trace)
        infra_failure = check_for_known_failure(job.trace)
        if not infra_failure.empty():
            setattr(job, "infra_failure", infra_failure)

        job_serialized_filename = os.path.join(pipeline_dir, f"job-{job.id}.pickle")
        with open(job_serialized_filename, "wb") as f:
            pickle.dump(job, f)

        if not infra_failure.empty():
            # In case of infrastructure failures, do not fetch the job
            # results. The artifacts can be huge in somecases, like
            # kernel hangs in which dEQP reports the entire master
            # case list as Missing = 133MB of useless data :)
            continue

        with open(os.path.join(pipeline_dir, f"job-results-{job.id}.csv"), "w") as f:
            in_memory_results = io.StringIO()
            results_scatter_list = [in_memory_results, f]
            if not get_first_found_artifact_bytes(project_config["results_filename_candidates"], pjob, results_scatter_list):
                ilog("\t\tFailed to get results.csv for %s %s" % (job.id, job.web_url))
                ilog("\t\tWARNING: This could be an unclassified infra failure")
                continue
            in_memory_results.seek(0)
            setattr(job, "deqp_results", deqp_results_from_csv(in_memory_results))
            with open(job_serialized_filename, "wb") as f:
                pickle.dump(job, f)

    mark_pipeline_cache_complete(pipeline_dir)


def pipeline_of_interest(pipe, project_config: dict):
    # The pipeline metadata doesn't include enough stuff for matching
    # pipelines of interest, the job objects do however, so grab one
    # for the user metadata. Quite inefficient, but this is the API we
    # have to work with.
    try:
        first_job = pipe.jobs.list()[0]
    except IndexError:
        return False

    if "branch_blacklist_pattern" in project_config:
        branch_blacklist_pat = project_config["branch_blacklist_pattern"]
        if re.match(branch_blacklist_pat, pipe.ref):
            ilog("Pipeline branch blacklisted: %s" % pipe.ref)
            return False

    if "branch_whitelist_pattern" in project_config:
        branch_whitelist_pat = project_config["branch_whitelist_pattern"]
        if re.match(branch_whitelist_pat, pipe.ref):
            ilog("Pipeline of interest: %s" % pipe.ref)
            return True

    if "email_pattern" in project_config:
        email_pat = project_config["email_pattern"]
        if re.match(email_pat, first_job.commit["author_email"]):
            ilog("Pipeline of interest: %s" % first_job.commit["author_email"])
            return True

    if "username_pattern" in project_config:
        username_pat = project_config["username_pattern"]
        if re.match(username_pat, first_job.commit["author_name"]):
            ilog("Pipeline of interest: %s" % first_job.commit["author_name"])
            return True

    if "commit_title_pattern" in project_config:
        commit_title_pat = project_config["commit_title_pattern"]
        if re.match(commit_title_pat, first_job.commit["title"]):
            ilog("Pipeline of interest (commit title): %s" % first_job.commit["title"])
            return True

    if "commit_message_pattern" in project_config:
        commit_message_pat = project_config["commit_message_pattern"]
        # Use re.DOTALL because commit messages contain newlines.
        if re.match(commit_message_pat, first_job.commit["message"], flags=re.DOTALL):
            ilog("Pipeline of interest (commit message):\n%s" % first_job.commit["message"])
            return True

    # Force explicit whitelisting
    return False


def get_cached_pipelines(cache_dir: str) -> list[str]:
    return [pipeline_key for pipeline_key in glob.glob("pipeline-*", root_dir=cache_dir) if os.path.exists(os.path.join(cache_dir, pipeline_key, ".complete"))]


def triage(proj, since: datetime, before: datetime, cache_dir: str, project_config: dict) -> datetime:
    cached_pipelines = get_cached_pipelines(cache_dir)
    latest_pipeline_triaged = None

    ilog("Fetching pipelines between %s and %s..." % (since, before), end=" ", flush=True)
    start_time = timeit.default_timer()
    pipelines = list(completed_pipelines(proj.pipelines.list(id=proj.id, updated_after=rfc3339_format(since), updated_before=rfc3339_format(before), all=True)))
    elapsed = timeit.default_timer() - start_time
    ilog("%.2f sec (%d pipelines)" % (elapsed, len(pipelines)), meta=False)
    local_pipes = []
    for pipe in pipelines:
        pipeline_key = f"pipeline-{pipe.id}"
        pipeline_dir = os.path.join(cache_dir, pipeline_key)

        if latest_pipeline_triaged is None:
            latest_pipeline_triaged = pipeline_created_at_iso(pipe)
        else:
            latest_pipeline_triaged = max(latest_pipeline_triaged, pipeline_created_at_iso(pipe))

        if pipeline_key in cached_pipelines:
            ilog("Already reported on pipeline %s (%s), skipping..." % (pipe.id, pipe.created_at))
            local_pipes.append(pipe)
            continue
        if not pipeline_of_interest(pipe, project_config):
            ilog("Skipping %s (branch=%s), it did not pass the branch filter checks" % (pipe.id, pipe.ref))
            os.makedirs(pipeline_dir)
            with open(os.path.join(pipeline_dir, ".complete"), "w") as f:
                f.write("Completed on %s" % datetime.now())
            continue

        # If a pipeline cache directory exists at this moment, it
        # means a previous cache attempt failed. Remove whatever
        # partial results we got and try again.
        shutil.rmtree(pipeline_dir, ignore_errors=True)
        os.makedirs(pipeline_dir, exist_ok=True)
        cache_pipeline(proj, pipe, pipeline_dir, project_config)
        local_pipes.append(pipe)

    return latest_pipeline_triaged, local_pipes


def generate_results(pipelines, cache_dir):
    results = defaultdict(list)
    for pipe in pipelines:
        pipeline_dir = os.path.join(cache_dir, f"pipeline-{pipe.id}")

        if not os.path.isdir(pipeline_dir):
            continue

        for pickled_job in glob.glob("job-*.pickle", root_dir=pipeline_dir):
            with open(os.path.join(pipeline_dir, pickled_job), "rb") as f:
                job = pickle.load(f)
            assert job
            job_key = generate_job_key(job)
            results[job_key].append(job)

    return results


def broadcast_lastest_results(results, project_config):
    pass


def cmd_triage(args: argparse.Namespace):
    config = config_from_args(args)
    if args.service_mode and os.path.isfile(".last-run.pickle"):
        with open(".last-run.pickle", "rb") as f:
            since = pickle.load(f)
    else:
        since = delta_since_from_args(args)
    before = before_from_args(args)

    try:
        project_config = config[args.project_name]
    except KeyError:
        print("ERROR: %s is not a known project, choices are %s" % (args.project_name, ",".join(config.keys())))
        return

    gl = gitlab.Gitlab(project_config["gitlab"]["instance_url"], project_config["gitlab"]["project_access_token"])
    proj = gl.projects.get(project_config["gitlab"]["project_id"])

    assert args.pipeline_cache_dir is not None
    check_pipeline_cache_disk_usage(args.pipeline_cache_dir, project_config["max_cache_size_in_bytes"])

    cache_dir = args.pipeline_cache_dir
    last_run_datetime, pipelines = triage(proj, since, before, cache_dir, project_config)

    if len(pipelines) == 0:
        print("Nothing to report since last fetch!")
        return

    results = generate_results(pipelines, cache_dir)

    if args.service_mode:
        broadcast_lastest_results(results, project_config)
        if last_run_datetime is None:
            last_run_datetime = datetime.utcnow()
        else:
            # If you keep the last pipeline created_at time, it will
            # continu to be fetched. Bumping by 1s isn't enough... I
            # think the Gitlab server granularity is 1 minute.
            last_run_datetime += timedelta(minutes=1)
        with open(".last-run.pickle", "wb") as f:
            pickle.dump(last_run_datetime, f)
    else:
        report_body = io.StringIO()
        job_formatter = JobFormatter(indent=1, colors=False)
        for runner_name, jobs in results.items():
            report_body.write("%s -> %s\n" % (runner_name, len(jobs)))
            report_body.write("%s\n" % job_formatter.fmt_header())
            for job in jobs:
                report_body.write("%s\n" % job_formatter.fmt(job))
        print(report_body.getvalue())

    push_results_to_influx(results, project_config)


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--config-file", required=True, type=str)
    parser.add_argument("--log-file", default=None, type=str)
    parser.add_argument("--log-level", default="DEBUG", type=str)
    subparsers = parser.add_subparsers()

    triage_parser = subparsers.add_parser(
        "triage",
        help="Fetch all pipelines in a given period, and generate a triaging report. Optionally notify administrators when job failures are encountered.",
    )
    triage_parser.add_argument(
        "--pipeline-cache-dir",
        type=PathType(exists=True, type="dir"),
        required=True,
        help="Local directory into which to cache the pipeline's job information from the remote Gitlab server",
    )
    triage_parser.add_argument(
        "--since",
        default="3 days",
        type=str,
        help="Only fetch information for pipelines since the given time. Format is 'N {minutes,hours,days,weeks}'.",
    )
    triage_parser.add_argument(
        "--service-mode",
        default=False,
        action="store_true",
        help="Keep track of the last pipeline fetch time, useful for daemon mode",
    )
    triage_parser.add_argument(
        "--before",
        default=datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S"),
        help="Only operate on pipelines updated before this date (inclusive). Format: %%Y-%%m-%%d or %%Y-%%m-%%dT%%H:%%M:%%S",
    )
    triage_parser.add_argument(
        "--project-name",
        type=str,
        required=True,
        help="The project name as specified in the configuration file",
    )
    triage_parser.set_defaults(func=cmd_triage)

    args = parser.parse_args()

    lvl = args.log_level.upper()
    if lvl not in LogLevel._member_names_:
        raise argparse.ArgumentTypeError("--log-level must be one of %s" % " or ".join(LogLevel._member_names_))
    global LOG_LEVEL
    LOG_LEVEL = getattr(LogLevel, lvl)

    if hasattr(args, "func"):
        args.func(args)
    else:
        parser.print_help()


if __name__ == "__main__":
    main()
