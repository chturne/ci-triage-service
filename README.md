# Overview

Currently this tool is purpose-build for AMD CI jobs, work will
continue to make it more generalised.

This tool can be run in one of the following ways,

  - In one-shot mode to report on a single pipeline
  - In triage mode to report over a time period
  - -In service mode to listen on Gitlab webhooks-

Run `./service.py --help` for detailed information.

If you wish to see a triage report for the last time period, do this,

```
./service.py --config-file config.yml  triage --since '2 week' --pipeline-cache-dir mesa-downstream-triage-cache --project mesa-downstream-amd-ci
```

As an example of the `config.yml` schema,

```
mesa-downstream-amd-ci:
  gitlab:
    instance_url: 'https://gitlab.freedesktop.org'
    project_access_token: 'SECRET'
    project_id: 9359 
  # Maximum size of local disk cache
  max_cache_size_in_bytes: 2147483648  # 2 GiB
  # List of branches to never bother checking.
  branch_blacklist_pattern: >-
    cturner/b2c-traces-runner
  # List of branches to focus on, others will be ignored.
  branch_whitelist_pattern: >-
    main
  # Pattern for interrogating only a subset of the pipelines jobs.
  job_name: >-
    ^deqp-.*
  job_stage: "amd"
  # List of file names relative to the artifacts root folder to
  # *attempt* to download. Feel free to put backwards-compat lists
  # here, the program will gracefully handle 404s. The first one that
  # matches will end the search.
  results_filename_candidates:
    - "job_folder/results/failures.csv"
    - "results/failures.csv"
  # These pipelines failed in one way or another that was not
  # interesting (development mistakes), do not report them.
  ignored_pipelines:
    - 516928
    - 508929
    - 502732
    - 516928
    - 517206
    - 517650
    - 518485
    - 518538
    - 518599
    - 518885
    - 519994
    - 519980
    - 518168
    - 518136
    - 517490
    - 517218
    - 491234
    - 491799
    - 491880
    - 465750
    - 491118
    - 502967
    - 491329
    - 464997
    - 432369
  ignored_jobs:
    - 17082042  # Unknown
    - 18014115  # name 'logger' is not defined  transient bug
    - 16737216  # Unknow
    - 16743492  # unknown what happened here..
    - 19092237  # YAML mistake
  # List of email addresses to send failure results to.
  notify_emails:
    - cturner@igalia.com
    - martin.peres@free.fr
  notify_irc:
    oftc:
      hostname: irc.oftc.net
      port: 6697
      secure: true
      botname: ci-bot
      channel: '#valve-ci-sync'
  influx:
    url: "https://influx.charles.plus"
    org: 'Valve Metrics'
    bucket: 'gitlab-metrics'
    access_token: "SECRET"
```

The program uses the local disk as a caching device. It will retrieve
pipeline results between a time range, skipping already retrieved
pipelines and jobs. It will then present a report showing the
evolution of a test case between the runs that have executed it, in
the hope of easier bisection.

In the case of a test failure, the program will email a pre-configured
list of users to save them from clicking around in the Gitlab instance.

The program can run in so called "service mode", in which case it will
record to the filesystem a last queried time, and on subsequent runs
will only fetch new pipelines. This is intended to be used as part of
a systemd service for example, where it would be triggered by a timer
every N minutes. In this mode, people are emailed about test results,

```
./service.py --config-file config.yml  triage --service-mode --pipeline-cache-dir mesa-downstream-triage-cache --project mesa-downstream-amd-ci
```

The notification aspects are WIP!

# Development Guide
None of the Python build / automation tools are used, because they all
complicate the process of development enormously.

Instead, use virtualenv's manually if you choose to develop, like so,

	pyenv virtualenv ci-triage-service
	pyenv activate ci-triage-service
    pip install --upgrade pip && pip install -r requirements.txt

	That's it, you'll be on the same page now.

Before committing code, run `pre-commit.sh`. You can set this up to
happen automatically in whatever way you like. It runs a code
formatter and various static analysis tools.
